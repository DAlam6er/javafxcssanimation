
package cssapplication;

import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Sphere;
import javafx.stage.Stage;
import javafx.util.Duration;

public class CssApplication extends Application
{

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        Button btn = new Button("Click me!");
        btn.setOnAction((event) -> {
           FadeTransition anim = new FadeTransition(Duration.seconds(3), btn);
           anim.setFromValue(1.);
           anim.setToValue(.0);
           anim.play();
        });
        
        // пустая коллекция(лист) со встроенным интерфейсом оповещения всем кому надо
        // о своем изменении
        ObservableList list = FXCollections.observableArrayList(); 
        // будем добавлять данные в коллекцию
        Button btn2 = new Button("Add");
        btn2.setOnAction((event) -> {
            list.add(new PieChart.Data("Data" + list.size(),
            new Random().nextInt(30)+10)); // от 10 до 40 будут колебаться случайные данные, добавляемые в коллекцию
        });
        
        FlowPane root = new FlowPane();
        root.getChildren().add(new Label("CSS is cool!"));
        root.getChildren().add(btn);
        root.getChildren().add(new Sphere(50));
        root.getChildren().add(btn2);
        root.getChildren().add(new PieChart(list)); // круговая диаграмма без имени, а значит к ней доступа нет!
        
                
        Scene scene = new Scene(root, 450, 500); // размеры окна из практики
        scene.getStylesheets().add(
            getClass().getResource("CascadeSS.css").toExternalForm());

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}